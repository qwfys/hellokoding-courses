package com.hellokoding.restfulapi.repository;

import com.hellokoding.restfulapi.model.Book;
import com.hellokoding.restfulapi.model.BookPublisher;
import com.hellokoding.restfulapi.model.Publisher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookPublisherRepositoryTests {

    @Autowired
    private BookPublisherRepository bookPublisherRepository;

    @Test
    public void contextLoads() {

    }


    @Test
    public void save() {
        Book san_guo = new Book("san guo");
        Book hong_lou_meng = new Book("hong lou meng");
        Book xi_you_ji = new Book("xi you ji");

        Publisher ji_xie_gong_ye = new Publisher("ji xie gong ye");
        Publisher dian_zi_gong_ye = new Publisher("dian zi gong ye");

        Date publishData = Calendar.getInstance().getTime();

        ArrayList<BookPublisher> bookPublishers = new ArrayList<>();

        //List<BookPublisher>.of((new BookPublisher(san_guo, ji_xie_gong_ye, publishData)),(new BookPublisher(xi_you_ji, ji_xie_gong_ye, publishData)));

        bookPublishers.add(new BookPublisher(san_guo, ji_xie_gong_ye, publishData));
        bookPublishers.add(new BookPublisher(xi_you_ji, ji_xie_gong_ye, publishData));
        bookPublishers.add(new BookPublisher(hong_lou_meng, dian_zi_gong_ye, publishData));

        bookPublisherRepository.saveAll(bookPublishers);


    }


}
