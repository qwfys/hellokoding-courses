package com.hellokoding.restfulapi.repository;

import com.hellokoding.restfulapi.model.BookPublisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface BookPublisherRepository extends JpaRepository<BookPublisher, Integer>, JpaSpecificationExecutor<BookPublisher>, QuerydslPredicateExecutor<BookPublisher> {
}
