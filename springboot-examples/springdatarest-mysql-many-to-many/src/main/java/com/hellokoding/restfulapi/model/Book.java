package com.hellokoding.restfulapi.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Book{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String title;

    private String description;

    @ManyToMany(cascade = CascadeType.ALL)
    //@JoinTable(
    //        name = "book_author",
    //        joinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id"),
    //        inverseJoinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id")
    //)
    private Set<Author> authors;



}
